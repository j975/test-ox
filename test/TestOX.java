/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author user
 */
public class TestOX {

    public TestOX() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCountWin() {
        Player o = new Player('O');
        o.countWin();
        assertEquals(true, o.getWin() == 1);
    }

    @Test
    public void testCountDraw() {
        Player x = new Player('X');
        x.countDraw();
        assertEquals(true, x.getDraw() == 1);
    }

    @Test
    public void testSwithTurn() {
        Player o = new Player('O');
        Player x = new Player('X');
        Board board = new Board(o, x);
        board.swithTurn();
        assertEquals(o, board.currentPlayer);
    }

    @Test
    public void testCheckCross1() {
        Player o = new Player('O');
        Player x = new Player('X');
        Board board = new Board(o, x);
        try {
            board.setRowCol(1, 1);
            board.setRowCol(2, 2);
            board.setRowCol(3, 3);
            assertEquals(true, board.checkWin(x, 0, 0));
        } catch (Exception e) {

        }

    }

    @Test
    public void testCheckRow() {
        Player o = new Player('O');
        Player x = new Player('X');
        Board board = new Board(o, x);
        try {
            board.setRowCol(1, 0);
            board.setRowCol(1, 1);
            board.setRowCol(1, 2);
        } catch (Exception e) {
            assertEquals(true, board.checkWin(x, 0, 0));
        }

    }

    @Test
    public void testCheckCol() {
        Player o = new Player('O');
        Player x = new Player('X');
        Board board = new Board(o, x);
        try {
            board.setRowCol(0, 2);
            board.setRowCol(1, 2);
            board.setRowCol(2, 2);
        } catch (Exception e) {
            assertEquals(true, board.checkWin(x, 0, 0));
        }

    }

    @Test
    public void testSetRowCol() {
        Player o = new Player('O');
        Player x = new Player('X');
        Board board = new Board(o, x);
        assertEquals(true, true);
    }

    @Test
    public void testSetName() {
        Player o = new Player('O');
        Player x = new Player('X');
        assertEquals('O', o.getName());
        assertEquals('X', x.getName());
    }

    @Test
    public void testRandomPlayer() {
        Player o = new Player('O');
        Player x = new Player('X');
        Board board = new Board(o, x);
        assertEquals(true, true);
    }

    @Test
    public void testGetData() {
        Player o = new Player('O');
        Player x = new Player('X');
        Board board = new Board(o, x);
        char data[][] = board.getData();
        assertEquals('-', data[0][0]);
    }
}
